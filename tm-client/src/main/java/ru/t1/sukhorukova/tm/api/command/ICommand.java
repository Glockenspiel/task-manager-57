package ru.t1.sukhorukova.tm.api.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

public interface ICommand {

    void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    String getArgument();

    @NotNull
    String getName();

    @NotNull
    String getDescription();

}
