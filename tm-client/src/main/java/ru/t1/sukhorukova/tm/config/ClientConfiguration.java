package ru.t1.sukhorukova.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.component.Bootstrap;

import javax.annotation.PostConstruct;

@Configuration
@ComponentScan("ru.t1.sukhorukova.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance();
    }

}
