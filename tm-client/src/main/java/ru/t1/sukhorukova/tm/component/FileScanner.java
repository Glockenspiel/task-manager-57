package ru.t1.sukhorukova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileScanner extends Thread {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner() {
        setDaemon(true);
    }

    public void init() {
        for (@Nullable final AbstractListener listener : listeners) {
            commands.add(listener.getName());
        }
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    publisher.publishEvent(new ConsoleEvent(fileName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

}
