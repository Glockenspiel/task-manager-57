package ru.t1.sukhorukova.tm.listener.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.sukhorukova.tm.listener.AbstractListener;
import ru.t1.sukhorukova.tm.enumerated.Role;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
