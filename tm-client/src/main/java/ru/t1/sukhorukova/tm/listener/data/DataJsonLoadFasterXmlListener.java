package ru.t1.sukhorukova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataJsonLoadFasterXmlRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    public static final String DESCRIPTION = "Load data in json file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD JSON]");

        getDomainEndpoint().jsonLoadFasterXmlData(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
