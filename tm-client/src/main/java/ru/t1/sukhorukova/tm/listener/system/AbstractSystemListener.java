package ru.t1.sukhorukova.tm.listener.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.listener.AbstractListener;
import ru.t1.sukhorukova.tm.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    private ISystemEndpoint systemEndpoint;

    protected ISystemEndpoint getSystemEndpoint() {
        return systemEndpoint;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
