package ru.t1.sukhorukova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sukhorukova.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Show version.";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");

        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(new ApplicationVersionRequest());
        @NotNull final String version = response.getVersion();

        System.out.println(version);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
