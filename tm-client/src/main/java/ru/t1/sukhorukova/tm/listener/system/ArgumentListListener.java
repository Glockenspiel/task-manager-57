package ru.t1.sukhorukova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.command.ICommand;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) continue;
            @Nullable final String argument = listener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
