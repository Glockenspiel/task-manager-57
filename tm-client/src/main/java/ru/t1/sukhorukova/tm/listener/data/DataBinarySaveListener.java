package ru.t1.sukhorukova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataBinarySaveRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE BINARY]");

        getDomainEndpoint().binarySaveData(new DataBinarySaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
