package ru.t1.sukhorukova.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.sukhorukova.tm.api.command.ICommand;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.ITokenService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Getter
@Setter
public abstract class AbstractListener implements ICommand {

    @Nullable
    @Autowired
    protected ITokenService tokenService;

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;

        return displayName;
    }

}
