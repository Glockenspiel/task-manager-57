package ru.t1.sukhorukova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.user.UserRemoveRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull private final String NAME = "user-remove";
    @NotNull private final String DESCRIPTION = "User remove.";

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REMOVE]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
