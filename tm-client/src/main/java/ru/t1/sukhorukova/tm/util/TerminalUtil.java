package ru.t1.sukhorukova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @Nullable final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
