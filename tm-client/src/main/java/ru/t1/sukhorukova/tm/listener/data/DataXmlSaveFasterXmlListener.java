package ru.t1.sukhorukova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataXmlSaveFasterXmlRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");

        getDomainEndpoint().xmlSaveFasterXmlData(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
