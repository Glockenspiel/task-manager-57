package ru.t1.sukhorukova.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.sukhorukova.tm.api.repository.dto.IProjectDtoDtoRepository;
import ru.t1.sukhorukova.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.sukhorukova.tm.api.service.dto.IProjectDtoService;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;
import ru.t1.sukhorukova.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

import static ru.t1.sukhorukova.tm.enumerated.ProjectSort.BY_NAME;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IProjectDtoDtoRepository> implements IProjectDtoService {

    public IProjectDtoDtoRepository getRepository() {
        return context.getBean(ProjectDtoRepository.class);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        return findAll(userId, BY_NAME);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);

        @NotNull final IProjectDtoDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId, sort);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setStatus(Status.NOT_STARTED);
        add(userId, project);
        return project;
    }

    @NotNull
    private ProjectDTO update(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final IProjectDtoDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return update(project);
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return update(project);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);

        return update(project);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);

        return update(project);
    }

}
