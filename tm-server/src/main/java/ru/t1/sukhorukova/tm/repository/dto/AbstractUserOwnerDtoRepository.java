package ru.t1.sukhorukova.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sukhorukova.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.sukhorukova.tm.dto.model.AbstractUserOwnerModelDTO;

import javax.persistence.EntityManager;

@Repository
@NoArgsConstructor
@Scope("prototype")
public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends AbstractDtoRepository<M> implements IUserOwnerDtoRepository<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
