package ru.t1.sukhorukova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sukhorukova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.sukhorukova.tm.api.service.IDomainService;
import ru.t1.sukhorukova.tm.dto.request.data.*;
import ru.t1.sukhorukova.tm.dto.response.data.*;
import ru.t1.sukhorukova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.sukhorukova.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64LoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64SaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse jsonLoadJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse jsonSaveJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse xmlLoadJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse xmlSaveJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
