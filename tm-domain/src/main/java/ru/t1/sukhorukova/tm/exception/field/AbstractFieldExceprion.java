package ru.t1.sukhorukova.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractFieldExceprion extends AbstractException {

    public AbstractFieldExceprion(@NotNull final String message) {
        super(message);
    }

    public AbstractFieldExceprion(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractFieldExceprion(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractFieldExceprion(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
