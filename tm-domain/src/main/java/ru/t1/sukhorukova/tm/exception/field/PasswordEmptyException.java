package ru.t1.sukhorukova.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldExceprion {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
