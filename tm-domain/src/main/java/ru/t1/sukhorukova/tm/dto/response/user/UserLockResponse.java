package ru.t1.sukhorukova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable UserDTO user) {
        super(user);
    }

}
