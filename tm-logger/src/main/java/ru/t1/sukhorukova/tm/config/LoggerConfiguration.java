package ru.t1.sukhorukova.tm.config;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.sukhorukova.tm")
public class LoggerConfiguration {

    @Bean
    public ConnectionFactory connectionFactory() {
        @NotNull final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        return connectionFactory;
    }

}
