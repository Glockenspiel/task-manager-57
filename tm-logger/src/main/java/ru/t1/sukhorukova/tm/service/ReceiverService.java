package ru.t1.sukhorukova.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.sukhorukova.tm.listener.EntityListener;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ReceiverService {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @SneakyThrows
    public void receive(@NotNull @Autowired EntityListener entityListener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
