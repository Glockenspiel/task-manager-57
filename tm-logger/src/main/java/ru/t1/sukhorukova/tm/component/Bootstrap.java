package ru.t1.sukhorukova.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.listener.EntityListener;
import ru.t1.sukhorukova.tm.service.EntityService;
import ru.t1.sukhorukova.tm.service.ReceiverService;

import javax.jms.*;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(entityListener);
    }

}
